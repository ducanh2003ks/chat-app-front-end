const ApiEndpoints = {
    SIGN_UP_URL: "api/user/signup/",
    LOGIN_URL: "api/user/login/",
    USER_URL: "users/",
    AUTH_USER_PROFILE: "users/auth/",
    POSTS: "posts/",
    POST_DETAIL:"posts/<post_id>/",
    ROOM_URL:"rooms/",
    USER_CHATS_URL:"users/<user_id>/chats",
    ROOM_MESSAGES_URL: "rooms/<room_id>/messages"

}

export default ApiEndpoints;