import Constants from "../lib/constants";
import HttpStatusCode from "./httpStatusCode";

const statusHandler = (response) =>{
    return response.status === HttpStatusCode.OK ||
    HttpStatusCode.CREATED 
    ? Promise.resolve(response)
    : Promise.reject(response);
}

const jsonHandler = (response) =>{
    return response.json()
}

const getPostRequestHeader = () =>{
    return {
        Accept: "application/json, text/plain",
        "Content-Type": "application/json; charset=utf-8",
    }
}

const getAuthHeader = () =>{
    const access_token = JSON.parse(localStorage.getItem("access_token"));

    return {
        Authorization: "Bearer " + access_token
    }
}

const ApiUtils = {
    statusHandler: statusHandler,
    jsonHandler: jsonHandler,
    getPostRequestHeader: getPostRequestHeader,
    getAuthHeader: getAuthHeader,
}

export default ApiUtils;