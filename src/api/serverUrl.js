const ServerUrl = {
    BASE_URL: 'https://nhat.vilayded.tech/',
    WS_BASE_URL: 'wss://nhat.vilayded.tech/',
}

export default ServerUrl;