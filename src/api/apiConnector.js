import ServerUrl from "./serverUrl";
import ApiUtils from "./apiUtils";
import HttpMethods from "./httpMethods";

const sendGetRequest = (relativeUrl) =>{
    const url = ServerUrl.BASE_URL + relativeUrl;
    const options = {
        headers: ApiUtils.getAuthHeader()
    }

    return fetch(url, options)
    .then(ApiUtils.statusHandler)
    .then(ApiUtils.jsonHandler)
    .then((data)=>
    {
        return data
    })  
    .catch((error)=> {
        console.error(error)

    })
}

const sendPostRequest = (relativeUrl, requestBody, isFormData=false) =>{
    const url = ServerUrl.BASE_URL + relativeUrl
    let bodyData;
    let headers = ApiUtils.getAuthHeader();

    // Check if the dataa is form-data, set body accordingly and do not set Content-Type
    // For form-data, the browser will set the correct Content-Type including boundary parameter

    if(isFormData && requestBody instanceof FormData) {
        bodyData = requestBody
    }else{
        // If not FormData, send as JSON
        headers['Content-Type'] = 'application/json'
        bodyData = JSON.stringify(requestBody)
    }
    const options = {
        method: 'POST',
        headers: headers,
        body: bodyData
    }

    return fetch(url, options)
    .then(ApiUtils.statusHandler)
    .then(ApiUtils.jsonHandler)
    .then((data) => {
        return data
    })
    .catch((error)=>
    {
        alert(error)
        console.error("error:",error)
    }
    )
        
}

const ApiConnector = {
    sendGetRequest: sendGetRequest,
    sendPostRequest: sendPostRequest,
}

export default ApiConnector