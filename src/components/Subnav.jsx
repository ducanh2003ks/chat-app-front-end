import React from "react";
import avatar1 from "../imgs/avatar1.jpg"
import CommonUtils from "../utils/commonUtils";
import AuthService from "../services/auth.service";
import { useNavigate } from "react-router-dom";
import AppPaths from "../lib/appPaths";
import logout from "../imgs/logout2.png"
import avatar2 from "../imgs/avatar10.jpg"
import avatar3 from "../imgs/avatar11.jpg"
import avatar4 from "../imgs/background1.jpg"

const Subnav = (props) =>{
    const {userAuth} = props
    const navigate = useNavigate();
    const handleLogout = ()=>{

        AuthService.logout()
        window.location.href = AppPaths.LOGIN
    }
    return (
        <div className="Subnav">
            <div className="header-subnav">
                <i className="ti-bell"></i>
                <img onClick={handleLogout} src={logout} alt="" />
            </div>
            <div className="body-subnav">
                
                <div className="form-avatar">
                    <img className="avatar" src={userAuth.user_image} alt="" />
                </div>
                <div className="info">
                    <p className="ten-nguoidung">{userAuth.first_name} {userAuth.last_name}</p>
                    <p className="id-nguoidung">@{userAuth.username}</p>
                </div>
                <div className="friendUser">
                    <div style={{display:"flex",flexDirection:"row",justifyContent:"space-between",marginRight:"10px",marginTop:"12px"}}>
                        <p className="titleFriend">Danh Sách Bạn Bè</p>
                        <p className="descFriend">Tất Cả</p>
                    </div>
                    
                   <div className="listfriendUser">
                        <div className="avatarFriend"><img src={avatar2} alt="" /></div>
                        <div className="infoFriend">
                            <p>Lê Quang Nhật</p>
                            <p className="desc">1 phút trước</p>
                        </div>
                   </div>
                   <div className="listfriendUser">
                        <div className="avatarFriend"><img src={avatar3} alt="" /></div>
                        <div className="infoFriend">
                            <p>Nguyễn Đức Anh</p>
                            <p className="desc">5 phút trước</p>
                        </div>
                   </div>
                   <div className="listfriendUser">
                        <div className="avatarFriend"><img src={avatar4} alt="" /></div>
                        <div className="infoFriend">
                            <p>Nguyễn Ai Đó</p>
                            <p className="desc">15 phút trước</p>
                        </div>
                   </div>
                            
                    
                </div>
            </div>
            <div className="footer-subnav">
                
            </div>

        </div>
    )
}

export default Subnav