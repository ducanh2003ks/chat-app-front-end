import React from "react";
import avatar1 from "../imgs/avatar1.jpg"

const Message = (props) =>{
    return (
        
        <div className="message" >
            <div className="messageInfo">
                <img src={avatar1} alt=""/>
                <span>just now</span>
            </div>
            <div className="messageContent">
                <p>
                    Message trai
                <i className="time-send">12:02</i>
                </p>
                
            </div>  
        </div>
        
    )
}

export default Message