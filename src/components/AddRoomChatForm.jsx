import React, {useState, useEffect, useRef} from "react";
import ApiConnector from "../api/apiConnector";
import ApiEndpoints from "../api/apiEndpoints";
import CommonUtils from "../utils/commonUtils";
import Constants from "../lib/constants";
import useDebounce from "../hooks/useDebounce";
import AppPaths from "../lib/appPaths";

export const AddRoomChatForm = (props) =>{
    const {setIsOpenAddChatRoomForm, onlineUserList} = props;
    const [searchedUsers, setSearchedUsers] = useState([])
    const [searchInput, setSearchInput] = useState('')
    const searchRef = useRef(false)
    const [showUserList, setShowUserList] = useState([])
    const [selectedUser, setSelectedUser] = useState()


    const debounce = useDebounce(searchInput, 500)
    useEffect(() =>{
        if (searchRef.current) {
            handleSearch();
        }
    }, [debounce])
    const cancelAddChatRoom = () =>{
        setIsOpenAddChatRoomForm(false);
    }

    const handleSearch = async() =>{
        const url = ApiEndpoints.USER_URL.replace(
          Constants.USER_ID_PLACE_HOLDER, 
          CommonUtils.getUserId()
        )
        const searchUrl = url + `?username=${encodeURIComponent(searchInput)}`;
        const searchUsers = await ApiConnector.sendGetRequest(searchUrl)
        setSearchedUsers(searchUsers)
        setShowUserList(true)
    }

    const selectUser = (user) =>
    {
        setSelectedUser(user)
        setSearchInput(user.username);
        setShowUserList(false);
    }
    const clearSelectedUser = () => {
        setSelectedUser(null);
        setSearchInput('');
      };

    const createRoom = async (e) =>{
        e.preventDefault();
        const idSelectedUser = selectedUser.id;
        const idAuthUser = CommonUtils.getUserId()
        const url = ApiEndpoints.ROOM_URL
        const requestBody = {
            type: "DM",
            connected_users: [idAuthUser, idSelectedUser]
          }
        
        const newChatUser = await ApiConnector.sendPostRequest(url, requestBody)
        if (newChatUser.error)
        {
            alert(newChatUser.error)
        }
    
        window.location.href = AppPaths.DEFAULT_CHAT
        cancelAddChatRoom()
    }
    
    return (
        <div className="add-room-chat-form-wrapper">
            <form className="form-wrapper" action=""
            onSubmit={(e)=>{
                createRoom(e)
            }}>
                <div className="form-control">
                    <div style={{display:"flex",flexDirection:"row",justifyContent:"space-between"}}>
                        <label className="form-control-label" htmlFor="partner">Tìm kiếm người dùng</label>
                    </div>
                    <input className="form-control-input" type="text" id="partner" name="partner" placeholder="Nhập tên người dùng..."
                    required
                    value={searchInput}
                    ref={searchRef}
                    onChange={(e) =>{
                        setSearchInput(e.target.value);
                        setShowUserList(true);
                    }}
                    onClick={()=>{
                        searchRef.current = true
                    }}
                    />
                </div>

                {selectedUser && (
                    <div className="selected-user">
                        <div className="selected-username">
                            {selectedUser.username}
                        </div>
             
                        <i className="ti-close"
                        onClick={() =>{
                        clearSelectedUser()
                        setShowUserList(false)
                        }}></i>
                   
                    </div>
                )}
                {showUserList && (
                    <ul className="user-list">
                    {searchedUsers.map((user) => (
                        <li className="user-list-item" key={user.id} onClick={() => {

                            selectUser(user)
                            setShowUserList(false); //
                        }
                        }>
                        {user.username}
                        </li>
                    ))}
                    </ul>
                )}
                <button className="add-room-submit"
                type="submit"
                >Kết nối</button>
                <button 
                    className="button-cancel"
                    onClick={cancelAddChatRoom}
                >
                    Đóng
                </button>
            </form>
        </div>
    )
}