import React from "react";
import { Route, BrowserRouter as Router ,Link} from 'react-router-dom';
import footerMenu from "../imgs/footerMenu.png"
import footerMenu2 from "../imgs/footerMenu2.png"
import diaNhac from "../imgs/diaNhac2.png"
import playNhac from "../imgs/playNhac2.png"
const NavbarHome = () =>{
    return (
        <div className="Navbar">
            <div className="header-nav"><h1 className="title-nav" style={{"fontWeight":"800"}}>Rhythm Romance</h1></div>
                <div className="body-nav">
                    <ul>
                        <li><a href="!#">Dành cho bạn</a></li>
                        <li><a href="!#">Nhạc thịnh hành</a></li>
                        <li><a href="!#">Nhạc trẻ Kpop</a></li>
                        <li><a href="!#">Nhạc bolero trữ tình</a></li>
                    </ul>
                </div>
                <div className="footerMenu">
                <img src={footerMenu} alt="" />
                <div style={{display:"flex"}}>
                    <div className="rotating-icon"><img src={diaNhac} alt="" /></div>
                    <div style={{display:"flex",gap:"16px",marginLeft:"28px",alignItems:"center",color:"rgb(55, 145, 155)"}}>
                        <i className="ti-control-skip-backward"></i>
                        <img style={{width:"18px",height:"18px",cursor:"pointer"}} src={playNhac} alt="" />
                        <i className="ti-control-skip-forward"></i>
                        <i className="ti-volume"></i>
                    </div>
                </div>

            </div>
        </div>
        
    )
}

export default NavbarHome