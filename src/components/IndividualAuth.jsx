import {React , useState, useEffect } from "react";
import Img1 from "../imgs/background1.jpg"
import Img2 from "../imgs/avatar1.jpg"
import axios from "axios";
import ApiConnector from "../api/apiConnector.js";
import ApiEndpoints from "../api/apiEndpoints.js";
import CommonUtils from "../utils/commonUtils.js";
const IndividualAuth = () =>{
    const [userAuth, setUserAuth] = useState(null)
    const getAuthUser = async () =>{
        const response = await ApiConnector.sendGetRequest(ApiEndpoints.AUTH_USER_PROFILE)
        setUserAuth(response)
    }
    useEffect(() =>{
        getAuthUser()
    }, [])

    if (!userAuth){
        return (
          <div>Loading ...</div>
        )
      }
    return (
        <div className="HomePage">
            
            <div style={{position:"relative"}}>
            <div className="topIndividual">
                <i className="ti-back-left"></i>
                <span>Trang cá nhân</span>
                <i className="ti-menu-alt"></i>
            </div>
            <div className="Individual" >
                <div className="headerIndividual">
                    <div className="imgIndividual">
                        <img className="coverImage" src={Img1} alt="" />
                        <i className="ti-camera icon-fixed"></i>
                        <div className="avatar">
                            <img src={Img2} alt="" />
                            <i className="ti-camera"></i>
                        </div>
                        <div className="infoIndividual">
                            <h2 className="name">{userAuth.first_name} {userAuth.last_name}</h2>
                            <p className="story">Tiểu sử của tôi</p>
                         </div>
                    </div>
                </div>
                <div className="bodyIndividual">
                    <div  className="navBodyIndividual">
                        <ul>
                            <li><a href="!#">Bài viết</a></li>
                            <li><a href="!#">Ảnh</a></li>
                        </ul>
                    </div>
                    <div className="contentIndividual">
                        {/* ---------------- form ----------------*/}
                        <div className="form-postIndividual">
                            <div className="sub-header">
                                <div style={{display:"flex",flexDirection:"row",alignItems:"center"}}>
                                <div className="icon-avatar">
                                    <img src={Img2} alt="" />
                                </div>
                                <div className="desc">
                                    <p>Nguyễn Thị Lan Anh đã đăng bài với tiêu đề là tiêu đề bài dăng</p>
                                    <span>12:25 | 21 thg 8</span>
                                </div>
                                </div>
                                
                                <i className="ti-more-alt"></i>
                            </div>
                            <div className="content-postIndiviual">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto, omnis optio magnam accusantium sapiente voluptatibus ab quas, cumque ipsum voluptas deleniti? Aperiam molestias nemo, quidem ratione eius aliquid repellat suscipit!</p>
                            </div>
                            <div className="img-area">
                                <img src={Img1} alt="" />
                                <div className="icon-invideo">
                                <li><i className="ti-heart"></i></li>
                                <li style={{marginBottom:"20px"}}><span>2323</span></li>
                                <li><i className="ti-comment"></i></li>
                                <li><span>23</span></li>
                            </div>
                        </div>  
                    </div>
                    {/* ---------------- form ----------------*/}
                
                    </div>
                </div>
           </div>
        </div>
        </div>
        
        
    )
}

export default IndividualAuth