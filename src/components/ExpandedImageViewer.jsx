import React, {useState, useEffect} from "react";

const ExpandedImageViewer = (props) =>{
    const {imageUrl, onClose} = props;

    return (
        <div className="expanded-image-viewer">
            <img src={imageUrl} alt="expanded file" className="expanded-image"/>
            <button onClick={() => window.open(imageUrl, '_blank')} className="download-button">
                Download
            </button>

            <button
            className="close-expanded-image-button"
            onClick={onClose}
            >
                Close
            </button>
        </div>
    )
}

export default ExpandedImageViewer;