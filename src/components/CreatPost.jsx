import React from "react";
import Img2 from "../imgs/avatar1.jpg"
import {useRef , useEffect ,useState} from "react";
import ApiConnector from "../api/apiConnector.js";
import ApiEndpoints from "../api/apiEndpoints.js";
import CommonUtils from "../utils/commonUtils.js";
import {useNavigate} from "react-router-dom";

const CreatPost = () =>{
  const [userAuth, setUserAuth] = useState(null)
  const navigate = useNavigate()
  const [title, setTitle] = useState("")
  const [caption, setCaption] = useState("")
  const [postAudio, setPostAudio] = useState(null)
  const [imageAudio, setImageAudio] = useState(null)
  const [imageUrl, setImageUrl] = useState(null)
  const [audioUrl, setAudioUrl] = useState(null)

  const getAuthUser = async () =>{
    const response = await ApiConnector.sendGetRequest(ApiEndpoints.AUTH_USER_PROFILE)
    setUserAuth(response)
  }

  const createPost = async () =>{
    const formData = new FormData()
    // formData.append("user_id", userAuth.id)
    formData.append("title", title)
    formData.append("caption", caption)
    formData.append("post_medias", postAudio)
    formData.append("post_images", imageAudio)
    try{
      const response = await ApiConnector.sendPostRequest(ApiEndpoints.POSTS, formData, true)
      console.log(response)
      navigate("/")
    }catch(error){
      console.log(error)
    }
  }
  useEffect(() =>{
    getAuthUser()
  }, [])


  
  if (!userAuth){
    return (
      <div>Loading ...</div>
    )
  }

  return (
      <div className="HomePage">
          <div className="topIndividual">
              <i className="ti-back-left"></i>
              <span>Tạo bài viết</span>
              <button onClick={createPost}>Đăng</button>
          </div>
          <div className="CreatPost">
              <div className="headerCreatPost">
                  <div className="icon-avatar">
                      <img src={userAuth.user_image} alt="" />
                  </div>
                  <div className="desc">
                      <p>{userAuth.first_name} {userAuth.last_name}</p>
                      <span>Bạn đang nghĩ gì ...</span>
                  </div>
              </div>
              <div className="bodyCreatPost">
                  <input className="inputTitlePost" 
                  type="text" 
                  placeholder="Tiêu đề của bạn ..." 
                  onChange={(e)=>{
                    setTitle(e.target.value)
                  }}
                  />
                  <input 
                  id="caption-post"
                  type="text" 
                  className="inputTitlePost" 
                  placeholder="Thêm mô tả cho post..."
                  onChange={(e)=>{
                    setCaption(e.target.value)
                  
                  }}
                  />
  
                  <div className="buttonFiles">
                      <ul>
                          <label style={{marginLeft:"10px"}} htmlFor="audio-upload" className="custom-file-upload">
                              Tải tệp âm thanh lên (bắt buộc)
                          </label>
                          <input type="file" id="audio-upload" 
                          accept="audio/*" 
                          style={{ display: 'none' }} 
                          onChange={(e)=>{
                            setPostAudio(e.target.files[0])
                            const audioUrl = URL.createObjectURL(e.target.files[0])
                            setAudioUrl(audioUrl)
                          }}
                          />
                          {audioUrl ? <audio style={{width:'200px', height:'40px'}} src={audioUrl} controls></audio> : <></>}
                          
                          <li><i className="ti-gallery icon-fixed-2"></i>
                            <label htmlFor="image-upload">
                              Hình ảnh
                            </label>
                            
                            </li>
                          <input type="file" id="image-upload" 
                          style={{ display: 'none' }} 
                          accept="image/*"
                          onChange={(e)=>{
                            setImageAudio(e.target.files[0])
                            const imageUrl = URL.createObjectURL(e.target.files[0])
                            setImageUrl(imageUrl)
                          }}
                          />
                          {imageUrl ? <img style={{width:'100px', height:'100px'}} src={imageUrl} alt="" /> : <></>}
                          
                          
                      </ul>
                  </div>
              </div>
            </div>
        </div>
       
        
    )
}

export default CreatPost