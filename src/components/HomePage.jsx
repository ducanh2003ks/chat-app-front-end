import HeaderHomePage from "../imgs/header-home-page.png"
import InfoPost from "../imgs/background1.jpg"
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import ReactDOM from 'react-dom/client';
import ApiConnector from "../api/apiConnector";
import ApiEndpoints from "../api/apiEndpoints";
import SlideShow from "../bass"
const HomePage = () =>{
    

    const [posts, setPosts] = useState([]);
    const fetchPosts = async () =>{
        try {
            const response = await ApiConnector.sendGetRequest(ApiEndpoints.POSTS);
        
            // Kiểm tra xem response có phải là mảng không
            if (Array.isArray(response)) {
              setPosts(response);
            } else {
              console.error("API response is not an array:", response);
            }
          } catch (error) {
            console.error("Error fetching posts:", error);
          }
    }
    useEffect(() => {
    
        fetchPosts();
    }, []);

    console.log("post:",posts)
    return (
        <div className="HomePage">
            <div className="header-homepage ">
                <input className="search" type="text" placeholder=" Tìm kiếm" />
                <SlideShow/>
                {/* <img className="img-header-homepage" src={HeaderHomePage} alt="" /> */}
            </div>
            <div style={{overflow:"scroll"}} className="body-homepage">
                {posts?.map((post,index) => (
                 <Link to={`/posts/${post.id}`} key={index}>
                    
                    <div className="form-post" >
                        <div className="img-post">
                            <img className="img-infopost" src={post.post_image[0].image} alt="" />
                        </div>
                        <div className="span">
                        <div className="info-post">
                            <h3 className="title-post">{post.title} </h3>
                            <p className="content-post">{post.caption}</p> 
                            {/* <h3 className="title-post">Hôm nay tôi buồn</h3> */}
                            {/* <p className="content-post">Ý tôi là buồn ngủ ah aha haha hahaah</p> */}
                            
                        </div> 
                        <div className="footer-post">
                            <p className="icon-post"><i style={{marginRight:"6px"}} className="ti-eye"></i>15366 </p>
                            <p style={{marginLeft:"20px"}} className="icon-post"><i style={{marginRight:"6px"}} className="ti-heart "></i>2019 </p>
                        </div>
                        </div>
                        <i className="hieuung"></i>
                    </div>
                
                </Link>
                )
                
                )}
                
               
            </div>
        </div>
    )
}

export default HomePage