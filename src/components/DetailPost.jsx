import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";
import Menu from "./Menu";
import NavbarHome from "./NavbarHome";
import Img1 from "../imgs/post1.png";
import ApiConnector from "../api/apiConnector";
import Constants from "../lib/constants";
import ServerUrl from "../api/serverUrl";
import ApiEndpoints from "../api/apiEndpoints";

const DetailPost = () => {
  const { postId } = useParams();
  const [post, setPost]= useState(null);
  const [postAudio, setPostAudio] = useState();
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef();
  const navigate = useNavigate();

  const fetchAPIPost = async (headers) => {
    try {
      const url = ApiEndpoints.POST_DETAIL.replace(
        Constants.POST_ID_PLACE_HOLDER,
        postId
      )
      const response = await ApiConnector.sendGetRequest(url)
      setPost(response)

    } catch (error) {
      console.error("Lỗi khi gọi API: ", error);
    }
  };

  useEffect(() => {
    fetchAPIPost()
    
  }, []);


  useEffect(()=>{

    if (post){
      setPostAudio(post.post_media[0].media_file)
    }
  },[post])

  if (!post){
    
    return <p>Loading...</p>;
  }


 
  // const postAudio2 = post.post_media.find((media) => media.is_chosen)?.media_file || null;
  // console.log("post au 2:", postAudio2)
  const playAudio = () => {
    if (isPlaying === false)
    {
      audioRef.current.play();
    }
    else
    {
      audioRef.current.pause();
    }
    setIsPlaying(!isPlaying);
  };

  const playPauseIcon = isPlaying ? "pause": "play";
 
  const backToPrevious = () => {
    navigate(-1); // back to previous
  }
  
  return (
        <>
      <div className="home">
        <div className="container">
          <Menu/>
          <NavbarHome />
          <div className="HomePage">
            <div className="music-page">
              <div className="wrapper">
                <div className="top-bar">
                  <i className="ti-back-left" onClick={backToPrevious}></i>
                  <span>Now Playing</span>
                  <i className="ti-menu-alt"></i>
                </div>
                <div className="body-playmusic">
                  <div className="img-area">
                    <img src={post.post_image[0].image} alt="" />
                    <div className="icon-invideo">
                      <li>
                        <i className="ti-heart"></i>
                      </li>
                      <li style={{ marginBottom: "20px" }}>
                        <span>{post.users_liked}</span>
                      </li>
                      <li>
                        <i className="ti-comment"></i>
                      </li>
                      <li>
                        <span>23</span>
                      </li>
                    </div>
                  </div>

                  <div className="song-details">
                    <p className="name"></p>
                    <p className="artist"></p>
                  </div>
                  <div className="progress-area">
                    <div className="progress-bar">
                      <audio id="main-audio" ref={audioRef} src={postAudio}></audio>
                    </div>
                    <div className="song-timer">
                      <span className="current-time">0:00</span>
                      <span className="max-duration">0:00</span>
                    </div>
                  </div>

                  <div className="function-playmusic">
                    <i id="repeat-plist" className="ti-reload" title="Playlist looped"></i>
                    <i id="prev" className="ti-control-backward"></i>
                    <div className="play-pause" onClick={playAudio}>
                      {playPauseIcon}
                      <i className="ti-control-play play"></i>
                    </div>
                    <i id="next" className="ti-control-forward"></i>
                    <i id="more-music" className="ti-volume"></i>
                  </div>

                  <div className="info-playmusic">
                    <h2>
                      {post.user.first_name} {post.user.last_name}<p>{post.users_liked} lượt thích</p>
                    </h2>

                    <ul>
                      <li>
                        <i className="ti-heart"></i>
                      </li>
                      <li>
                        <i className="ti-comments"></i>
                      </li>
                      <li>
                        <i className="ti-flag-alt"></i>
                      </li>
                    </ul>
                  </div>
                  <div className="content-playmusic">
                    <h2>{post.title}</h2>
                    <p>{post.caption}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        </>

  );
};

export default DetailPost;