import React from "react";
import { Route, BrowserRouter as Router,Link } from 'react-router-dom';


const NavbarIndividual = () =>{
    return (
        <div className="Navbar">
            <div className="header-nav"><h1 className="title-nav" style={{"fontWeight":"800"}}>Rhythm Romance</h1></div>
                <div className="body-nav">
                    <ul>
                        <Link to="/Individual/IndividualAuth"><li><a href="/Individual/IndividualAuth"><i className="ti-user icon-nav"></i>Trang cá nhân</a></li></Link>
                        <Link to="/Individual/CreatPost1"><li><a href="/Individual/CreatPost1"><i className="ti-music-alt icon-nav"></i>Tạo bài đăng</a></li></Link>
                        <Link to=""><li><a href="!#"><i className="ti-thought icon-nav"></i>Đang theo dõi</a></li></Link>
                    </ul>
                </div>
                
        </div>
        
    )
}

export default NavbarIndividual