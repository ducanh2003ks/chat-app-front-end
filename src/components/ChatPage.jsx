import React from "react";
import Input from "./Input.jsx";
import Cam from "../imgs/cam.png"
import Add from "../imgs/add.png"
import More from "../imgs/more.png"
import Messages from "./Messages.jsx";
import {w3cwebsocket as WebSocket} from 'websocket'
import { useRef, useState, useEffect } from "react";
import axios from "axios";
import ServerUrl from "../api/serverUrl.js";
import CommonUtils from "../utils/commonUtils.js";
import ApiEndpoints from "../api/apiEndpoints.js";
import Constants from "../lib/constants.js";
import ApiConnector from "../api/apiConnector.js";
import avatar1 from "../imgs/avatar1.jpg"
import Img from "../imgs/img.png";
import Attach from "../imgs/attach.png";
import SocketActions from "../lib/socketActions.js";
import PrivateLiveVideoCall from "../pages/VideoCall/PrivateLiveVideoCall.js";
import {getDownloadURL, ref, uploadBytes} from "firebase/storage";
import {addDoc, collection, serverTimestamp, onSnapshot, query, where, orderBy} from "firebase/firestore";
import {auth, db, storage} from "../firebase/config.js";
import {v4} from "uuid"
import ArticleIcon from '@mui/icons-material/Article';
import ExpandedImageViewer from "./ExpandedImageViewer.jsx";

// Kết nối socket
let typingTimer = 0;


let socket;

if (CommonUtils.getUserId()) {

    socket = new WebSocket(ServerUrl.WS_BASE_URL + `ws/users/${CommonUtils.getUserId()}/chat/`);
}else{
    socket = new WebSocket(ServerUrl.WS_BASE_URL)
}

let callSocket;
callSocket = new WebSocket(ServerUrl.WS_BASE_URL + `ws/users/video_call/`);
let isTypingSignalSent = false;
const ChatPage = ({match, currentChattingMember, setOnlineUserList}) =>{
    const [inputMessage, setInputMessage] = useState("")
    const [messages, setMessages] = useState([])
    const [typing, setTyping] = useState(false)

    const messagesContainerRef = useRef(null);

    const [isVideoCallActive, setIsVideoCallActive] = useState(false);
    const [isCaller, setIsCaller] = useState(false);

    const [fileImage, setFileImage] = useState(null);

    const messagesFirestoreRef = collection(db, "room-messages")

    const [isImageExpanded, setIsImageExpanded] = useState(false);
    const [expandedImageUrl, setExpandedImageUrl] = useState(null);

    // Lấy tất cả tin nhắn trong room có id trong props.match
    const fetchChatMessage = async () =>{
        const currentChatId = CommonUtils.getActiveChatId(match)
        if (currentChatId) {
            const url = ApiEndpoints.ROOM_MESSAGES_URL.replace(
                Constants.ROOM_ID_PLACE_HOLDER,
                currentChatId
            )+"?limit=20&offset=0";
            try{

                const chatMessages = await ApiConnector.sendGetRequest(url);
                if(Array.isArray(chatMessages))
                {
                    const formattedChatMessages = CommonUtils.getFormattedMessages(chatMessages)
                    return formattedChatMessages;
                }else{
                    console.error("API response is not an array:", chatMessages);
                }
            }catch(e) {
                console.error(e)
            }
            // setMessages(formattedChatMessages)
        }
        return []
    }

    const fetchChatMessageFirestore = async () =>{
        const chatMessagesDjango = await fetchChatMessage();

        const queryMessages = query(messagesFirestoreRef, where("roomId", "==", CommonUtils.getActiveChatId(match), orderBy("timestamp")));
        return onSnapshot(queryMessages, (snapshot) => {

            const chatMessagesFirestore = [];
            snapshot.forEach((doc) => 
            {
                try{
                    const date = doc.data().timestamp.toDate();
                    
                    chatMessagesFirestore.push({
                    id: doc.id,
                    user: doc.data().user,
                    file: doc.data().file,
                    message: CommonUtils.handleGetMessage(doc.data().message),
                    user_image: doc.data().user_image,
                    timestamp: date.toISOString() // Firestore Timestamp to milliseconds
                  });
                }catch(err)
                {
                    console.log(err)
                }
        
            });
            // Combine messages from Django and Firestore
            const combinedMessages = [...chatMessagesDjango, ...chatMessagesFirestore];
        
            // Sort combined messages by timestamp
            combinedMessages.sort((a, b) => a.timestamp.localeCompare(b.timestamp));
            setMessages(combinedMessages); // Update the state with sorted messages
        
          });
        
    }

    // Mount các tin nhắn tương ứng với id room
    useEffect(()=>{
        fetchChatMessageFirestore();
    }, [CommonUtils.getActiveChatId(match)])

    const loggedInUserId = CommonUtils.getUserId()
    const getChatMessageClassName = (userId) =>{
        return loggedInUserId === userId ? "owner":"message";
    }

    socket.onmessage = (event)=>{
        const data = JSON.parse(event.data);
        const chatId = CommonUtils.getActiveChatId(match)
        const userId = CommonUtils.getUserId()

        if(chatId === data.room_id)
        {
            if(data.action === SocketActions.MESSAGE)
            {
                // Xóa / ở cuối url
                data['user_image'] = data.user_image;
                data['message'] = CommonUtils.handleGetMessage(data.message)
                const new_messages = [...messages, data]
                setMessages(new_messages)
                if (data.user !== userId)
                {
                    setTyping(false)
                }

            }else if (data.action === SocketActions.TYPING && data.user !== userId)
            {
                setTyping(data.typing)
            }
        }

        if (data.action === SocketActions.ONLINE_USER)
        {
            setOnlineUserList(data.user_list)
        }
    }

    const handleFileImageSubmit = async (file)=>{
        if(!file)
        {
            return
        }

        const storageRef = ref(storage, `files-bucket/${v4()}/${file.name}`)
        // upload
        const snapshot = await uploadBytes(storageRef, file)
        // get the file url
        const downloadURL = await getDownloadURL(snapshot.ref)

        // send file url to firestore

        await addDoc(messagesFirestoreRef, 
            {
                user_image: CommonUtils.getUserImage(),
                user: CommonUtils.getUserId(),
                file: downloadURL,
                message: CommonUtils.encryptMessage(inputMessage),
                timestamp: serverTimestamp(),
                roomId: CommonUtils.getActiveChatId(match),
            }
        )

        setFileImage(null)
        sendTypingSignal(false)
        
    }
    const messageSubmitHandler = async (event) =>{
        event.preventDefault();
        if(inputMessage.trim()!=="" || fileImage)
        {
            if (fileImage)
            {
                handleFileImageSubmit(fileImage)
            }else{

                await socket.send(
                    JSON.stringify({
                        action: SocketActions.MESSAGE,
                        message: CommonUtils.encryptMessage(inputMessage),
                        user: CommonUtils.getUserId(),
                        room_id: CommonUtils.getActiveChatId(match)
    
                    })
                )
            }
        }
        setInputMessage("")
    }

    const sendTypingSignal = (typing) =>{
        socket.send(
            JSON.stringify({
                action: SocketActions.TYPING,
                typing: typing,
                user: CommonUtils.getUserId(),
                room_id: CommonUtils.getActiveChatId(match)
            })
        )
    }

    const chatMessageTypingHandler = (event) =>{
        if(event.keyCode !== Constants.ENTER_KEY_CODE)
        {
            if (inputMessage !== "")
            {
                sendTypingSignal(true)
                isTypingSignalSent = true;
            }else{
                sendTypingSignal(false)
            }

        }else{
            isTypingSignalSent = false;
            setTyping(false)
        }
    }
    const connectSocket = () =>{
        const caller = CommonUtils.getAuthUsername();
        callSocket.send(JSON.stringify({
          type:'login',
          data:{
            name: caller
          }
        }))
  
    }
    callSocket.onopen = () =>{
        connectSocket();
    }
    callSocket.onmessage =(event)=>{
        let response = JSON.parse(event.data)
        let type = response.type;
        if (type == 'receive_call')
        {
            setIsVideoCallActive(!isVideoCallActive)
        }
    }

    const sendCall = () =>{
        callSocket.send(JSON.stringify({
            type: 'caller_call',
            data:{
                receiver: currentChattingMember.name 
            }
        }))
    }
    const handleToggleVideoCall = () => {
        if (currentChattingMember.isOnline == true)
        {
            setIsVideoCallActive(!isVideoCallActive);
            setIsCaller(!isCaller);
            sendCall();
        }
        else{
            alert(`${currentChattingMember.name} is not Online! Try later!`)
            return;
        }
        // const url = '/videotest';
        // window.open(url, 'New Window', 'width=1000, height=800')
      };

    const deleteFileInputSend = () => {
        setFileImage(null)

    }

    const checkFileExtension = (url, extensions) => {
        return extensions.some(extension => url?.toLowerCase().includes(`.${extension}`));
    }
    
    const determineFileType = (url) => {
        const imageExtensions = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'tiff', 'webp', 'heic'];
        const videoExtensions = ['mp4', 'avi', 'mov', 'mkv', 'wmv'];
        const audioExtensions = ['mp3'];  // Thêm đuôi mở rộng MP3 vào đây
    
        if (checkFileExtension(url, imageExtensions)) {
            return 'image';
        } else if (checkFileExtension(url, videoExtensions)) {
            return 'video';
        } else if (checkFileExtension(url, audioExtensions)) {
            return 'audio';
        } else {
            return 'other';
        }
    }
    
    const handleImageClick = (url) =>{
        // setIsImageExpanded(!isImageExpanded)
        setExpandedImageUrl(url)
    }

    const closeExpandedImage = () =>{
        setExpandedImageUrl(null)
    }

    useEffect(() =>{
        if (messagesContainerRef.current) {
            messagesContainerRef.current.scrollTop = messagesContainerRef.current.scrollHeight;
        }
        
        
    },[messages])

    useEffect(()=>{
        if (isVideoCallActive == true)
        {
            document.querySelector('.body-homepage').style.display = 'none';
        }
    },[])
    

    return (
        <div style={{margin:"0 0",width:"656px"}} className="HomePage">
            {isVideoCallActive&& 
            <PrivateLiveVideoCall setIsVideoCallActive={setIsVideoCallActive} caller={CommonUtils.getAuthUsername()} isCaller={isCaller} otherUser={currentChattingMember.name} callSocket={callSocket}/>}
            
            {currentChattingMember && 
            <div  style={{height:"500px",width:"656px",padding:"0 0",flexDirection:"column",gap:"0px"}}  className="body-homepage">
                <div className="chat">
                    <div className="chatInfo">
                        <div className="chatAvatar">
                            <div className="avatar-chat">
                                <img src={currentChattingMember.image} alt="" />
                            </div>
                            
                        <span>{currentChattingMember&& currentChattingMember.name || "" }</span>
                        </div>
                        <div className="chatIcons">
                        <img src={Cam} alt="" onClick={handleToggleVideoCall} />
                        <img src={Add} alt="" />
                        <img src={More} alt="" />
                        </div>
                    </div>
                </div>
                {/* Hiển thị VideoRoom nếu isVideoCallActive là true */}
                
                
                <div className="messages" ref={messagesContainerRef}>
                    
                   
                    {/* message trai */}
                    {messages && messages?.map((message, index) =>{
                        const fileType = determineFileType(message.file);
                        const decodedUrl = decodeURIComponent(message.file?.split('/').pop().split('?')[0])
                        const fileParts = decodedUrl.split('/')
                        const fileName = fileParts[fileParts.length - 1]
                        return (
                            <div key={index} className={getChatMessageClassName(message.user)} >
                                <div className="messageInfo">
                                    <img src={message.user_image} alt="image" />
                                </div>
                                <div className="messageContent">
                                    <p>{message.message}</p>
                                    {message.file && (
                                        <div className="file-content">
                                            {fileType === 'image' ? (
                                                <div style={{cursor:'pointer'}}>
                                                    <img 
                                                    src={message.file} 
                                                    alt='sent file' 
                                                    style={{ maxWidth: '200px' }} 
                                                    onClick={() =>{
                                                        handleImageClick(message.file);
                                                    }}
                                                    
                                                    />
                                                    {expandedImageUrl === message.file && (
                                                        <ExpandedImageViewer imageUrl={message.file} onClose={closeExpandedImage}/>
                                                    )}

                                                </div>
                                            ) : fileType === 'video' ? (
                                                <video controls width="200" height="150">
                                                    <source src={message.file} type="video/mp4" />
                                                    Your browser does not support the video tag.
                                                </video>
                                            ) : fileType === 'audio' ? (
                                                <audio controls>
                                                    <source src={message.file} type="audio/mp3" />
                                                    Your browser does not support the audio tag.
                                                </audio>
                                            ) : (
                                                <div className="file-non-image">
                                                    <ArticleIcon />
                                                    <a href={message.file} download target="_blank" rel="noopener noreferrer">
                                                        {fileName}
                                                    </a>
                                                </div>
                                            )}
                                        </div>
                                    )}
                                    <i className="time-send">{message.timestamp}</i>
                                </div>
                            </div>
                        )}
                    )}
                     {typing && (
                        <div className="message flex-dots">
                            <div className="dot">.</div>
                            <div className="dot">.</div>
                            <div className="dot">.</div>
                        </div>
                    )}
                    
                   
                </div>
                {/* Input Send */}
                
                
                <form action="" onSubmit={messageSubmitHandler}>
                    <div className="input">
                        {fileImage? (
                            <div className="file-send">
                                <input 
                                id="file-name"
                                className="file-send-name"
                                type="text"
                                value={fileImage.name}
                                readOnly
                                />
                                <div 
                                className="file-send-delete"
                                onClick={deleteFileInputSend}>
                                    X
                                </div>
                            </div>

                        ): 
                        <></>} 
                        <input
                            id="input-message"
                            type="text"
                            placeholder="Nhập gì đó đi..." 
                            onChange={(event) => setInputMessage(event.target.value)}
                            onKeyUp={chatMessageTypingHandler}
                            value={inputMessage}
                           
                        />

                        <div className="send">
                            <input
                                type="file"
                                style={{ display: "none" }}
                                id="file"
                                onChange={(e)=>{
                                    setFileImage(e.target.files[0]);
                                }}
                            />
                            <label htmlFor="file">
                            <img src={Attach} alt="" />
                            </label>

                            <button
                                id="chat-message-submit"
                                type="submit"
                            >
                                Send
                            </button>
                        </div>
                    </div>
                </form>
            
            </div>
            || <div></div>
            }
        </div>
    )
}

export default ChatPage