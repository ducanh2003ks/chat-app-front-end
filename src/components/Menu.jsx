import React from "react";
import BrButton from "../imgs/BrowserButton.png"
import { Link } from 'react-router-dom';
import AppPaths from "../lib/appPaths";
import iconHome from "../imgs/home.png"
import iconChat from "../imgs/messenger.png"
import iconPlus from "../imgs/plush.png"
import iconSupport from "../imgs/support.png"


const Menu = () =>{
    return (
        <div className="Menu">
            <div className="body-menu icon-container">
                <img className="BrButton" src={BrButton} alt="" />
                {/* <img className="Logo" src={Logo} alt="" /> */}
                <ul className="aaaaa">
                <Link to ={AppPaths.HOME}><li>
                    <img className="iconMenuFix" src={iconHome} alt="" />
                </li></Link> 
                <a style={{color:"white"}} href={AppPaths.DEFAULT_CHAT}><li>
                    <img className="iconMenuFix" src={iconChat} alt="" />
                </li></a> 
                <Link to="/Individual"><li ><img className="iconMenuFix addItem" src={iconPlus} alt=""/></li></Link>
                {/* <li class="content"><img src="./img/creatPost.png"></li>
                <li class="content"><img src="./img/messenger.png"></li>
                <li class="content"><img class="addItem" src="./img/add.png" alt=""></li> */}
                    {/* <Link to={AppPaths.HOME}><li style={{color:"white"}}><i className="ti-home"></i></li></Link>
                    <a style={{color:"white"}} href={AppPaths.DEFAULT_CHAT}><li><i className="ti-comments"></i></li></a>
                    <Link to="/Individual"><li><a href="/Individual"><i className="ti-plus"></i> </a></li></Link> */}
                </ul>
            </div>
           <div className="footerM">
                <i className="ti-settings"></i>
                {/* <img style={{marginTop:"13px"}} src={iconSupport} alt="" /> */}
           </div>
        </div>
    )
}

export default Menu