import React, { useState, useEffect } from "react";
import avatar1 from "../imgs/avatar1.jpg";
import ChatPage from "../components/ChatPage.jsx";
import axios from "axios";
import { useRef} from "react";
import AppPaths from "../lib/appPaths";
import CommonUtils from "../utils/commonUtils";
import ApiEndpoints from "../api/apiEndpoints";
import Constants from "../lib/constants";
import ApiConnector from "../api/apiConnector";
import { Link } from "react-router-dom";
import ApiUtils from "../api/apiUtils";
import useDebounce from "../hooks/useDebounce.js";
import {getDownloadURL, ref, uploadBytes} from "firebase/storage";
import {addDoc, collection, serverTimestamp, onSnapshot, query, where, orderBy} from "firebase/firestore";
import {auth, db, storage} from "../firebase/config.js";
import {v4} from "uuid"
import {useNavigate} from "react-router-dom";

const NavbarChat = (props) => {
  // sidebar users, chứa thông tin về các cuộc trò chuyện người dùng tham gia
  const [chatUsers, setChatUsers] = useState([])
  // Popup users
  const [users, setUsers] = useState([])
  const [isShowAddPeopleModal, setIsShowAddPeopleModal] = useState(false)
  const [searchInput, setSearchInput] = useState()
  const [userSearchList, setUserSearchList] = useState([])
  const isClickSearch = useRef(false)
  const [isSearchActive, setIsSearchActive] = useState(false);
  const searchRef = useRef(null);
  const listRef = useRef(null);

  const debounce = useDebounce(searchInput, 500)
  const navigate = useNavigate();
  // xác định và thiết lập người dùng đang tham gia cuộc trò chuyện
  // mặc định hoặc cuộc trò chuyện hiện tại dựa trên đường dẫn của ứng dụng
  const redirectUserToDefaultChatRoom = (chatUsers)=>{
    if (props?.location?.pathname == AppPaths.DEFAULT_CHAT)
    {
      // Nếu là default chat thì điều hướng đến trang chat gần nhất
        props.setCurrentChattingMember(chatUsers[0])
        // window.location.href = "/c/"+chatUsers[0].room_id
        navigate("/c/"+chatUsers[0].room_id)
    }else{
      const activeChatId = CommonUtils.getActiveChatId(props.match)
      const chatUser = chatUsers?.find((user)=> user.roomId === activeChatId);
      props.setCurrentChattingMember(chatUser)
    }
  }

  const fetchChatUser = async () =>{
    const url = ApiEndpoints.USER_CHATS_URL.replace(
      Constants.USER_ID_PLACE_HOLDER, 
      CommonUtils.getUserId()
    )
    
    const chatUsers = await ApiConnector.sendGetRequest(url)

    const formatedChatUser = CommonUtils.getFormatedChatUsers(
      chatUsers, props.onlineUserList
    )
    setChatUsers(formatedChatUser)
    redirectUserToDefaultChatRoom(formatedChatUser)
  }

  useEffect(()=>{
    fetchChatUser()
  }, [])

  const handleClickOutside = (event) =>{
    if (
      searchRef.current &&
      listRef.current &&
      !searchRef.current.contains(event.target) &&
      !listRef.current.contains(event.target)
    ) {
      setIsSearchActive(false);
    }
  }
  useEffect(()=>{
    if (searchRef.current)
    {
      handleSearch();
    }
  },[debounce])

  useEffect(() =>{
     // Add the event listener for mousedown or click
     document.addEventListener('mousedown', handleClickOutside);
     // Remove the event listener on cleanup
     return () => {
       document.removeEventListener('mousedown', handleClickOutside);
     };
  },[searchRef, listRef])

  
  const getChatListWithOnlineUser = (chatUsers) =>{
    let updatedChatList = chatUsers.map((user) =>{
      if (props.onlineUserList.includes(user.id)){
        user.isOnline = true;
      }else{
        user.isOnline = false;
      }
      return user;
    })
    return updatedChatList;
  }

  const handleSearch = async() =>{
    const url = ApiEndpoints.USER_CHATS_URL.replace(
      Constants.USER_ID_PLACE_HOLDER, 
      CommonUtils.getUserId()
    )
    const searchUrl = url + `?username=${encodeURIComponent(searchInput)}`;
    const searchUsers = await ApiConnector.sendGetRequest(searchUrl)
    const formattedSearchUsers = CommonUtils.getFormatedChatUsers(searchUsers, props.onlineUserList)
    setChatUsers(formattedSearchUsers)
  }


  const createRoom = async (key) =>{
    const url = ApiEndpoints.ROOM_URL
    const idAuthUser = CommonUtils.getUserId()
    const idOtherUser = key
    const requestBody = {
      type: "DM",
      connected_users: [idAuthUser, idOtherUser]
    }
    const newChatUser = await ApiConnector.sendPostRequest(url, requestBody)
    const formatedNewChatUser = CommonUtils.getFormatedOneChatUser(newChatUser, props.onlineUserList)
    const formatedNewChatUsers = [...chatUsers, formatedNewChatUser]
    setChatUsers(formatedNewChatUsers);
  }

  const openAddChatRoomForm = () =>{
    props.setIsOpenAddChatRoomForm(true)
  }

  if(!chatUsers)
  {
    return (
      <div>Loading ..</div>
    )
  }

  return (
    <>
      <div className="Navbar">
        <div className="header-nav">
          <h1 className="title-nav" style={{"fontWeight":"800"}}>Rhythm Romance</h1>
        </div>
        <div className="search-nav">
          <input className="search-user" 
          ref={searchRef}
          value={searchInput}
          onChange={(e)=>
            {
              setSearchInput(e.target.value);
            }
          }
          onClick={() =>{

            searchRef.current = true;
          }
          }

          type="text" placeholder="Tìm kiếm người dùng ..." />
          <i className="ti-search"></i>
          <i className="ti-plus" onClick={openAddChatRoomForm}></i>
          
          {isSearchActive&& (
          <div className="list-search-users" ref={listRef}>
            {
              userSearchList?.map((user)=>{
                return (
                  <div 
                  className="search-user-item" 
                  onClick={(e)=>{
                    createRoom(user.id)
                    isClickSearch.current = false;
                    setUserSearchList([])
                  }} 


                  key={user.id}>
                    {user.username}
                  </div>
                )
              })
            }
          </div>
          )
          }
        </div>
        <div className="nav-bodynav">
          <a className="nav-all">Tất cả</a>
          <a>Chưa đọc</a>
        </div>
        <div className="body-nav">
        {getChatListWithOnlineUser(chatUsers)?.map((chatUser)=>{
          return (
            <Link
            onClick={()=>{
              props.setCurrentChattingMember(chatUser)
              props.setIsOpenChatRoom(true)
            }}
            to={`/c/${chatUser.roomId}`}
            key={chatUser.id}
            >
              <div className="user-chat">
                <div className="form-avatarnav">
                  <img className="avatar-navchat" src={chatUser.image} alt="Unknown" />
                </div>
                <div className="userchatinfo">
                  <div style={{display:"flex",justifyContent:"flex-end"}}>
                    <p className="trangThai" style={{ color: chatUser.isOnline ? "green" : "red" }}>
                     {chatUser.isOnline ? "Online" : "Offline"}
                    </p>
                  </div>
                  <div style={{display:"flex",flexDirection:"column"}}>
                    <p className="name">{chatUser.name}</p>
                    <p className="chat-late">
                     {CommonUtils.handleGetMessage(chatUser.latestMsg)}
                    </p>
                  </div>
                  
                 
                </div>
              </div>
            </Link>
          )
        })}

        </div>
      </div>
    </>
  );
};

export default NavbarChat;
