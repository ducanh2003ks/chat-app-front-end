import React from "react";
import Img from "../imgs/img.png";
import Attach from "../imgs/attach.png";
import { useState } from "react";
const Input = () =>{

    return (
        <div className="input">
            <input
                id="input-message"
                type="text"
                placeholder="Nhập gì đó đi..." 
               
            />
            <div className="send">
                <img src={Attach} alt="" />
                <input
                type="file"
                style={{ display: "none" }}
                id="file"
                />
                <label htmlFor="file">
                <img src={Img} alt="" />
                </label>
                <button>
                    Send
                </button>
            </div>
        </div>
    )
}

export default Input