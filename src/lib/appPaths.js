const AppPaths = {
    HOME:"/",
    CHAT_PAGE:"/chatpage",
    DEFAULT_CHAT:"/default/chat",
    CHAT_ROOM:"/c/:chatId",
    LOGIN: "/login",
    SIGN_UP: "/signup"
}

export default AppPaths;