const Constants = {
    USER_ID_PLACE_HOLDER:"<user_id>",
    ROOM_ID_PLACE_HOLDER:"<room_id>",
    POST_ID_PLACE_HOLDER:"<post_id>",
    ENTER_KEY_CODE: 13,
    TOKEN: 'access_token'
}


export default Constants;