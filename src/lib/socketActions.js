const SocketActions = {
    MESSAGE: "message",
    TYPING: "typing",
    ONLINE_USER: "online_user"
}
export default SocketActions;