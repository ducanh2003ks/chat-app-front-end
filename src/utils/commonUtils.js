import {jwtDecode as jwt_decode} from 'jwt-decode'
import {encrypt, decrypt} from 'n-krypta'


const getFormattedMessages = (messages) =>{
    return messages.reduce((acumulator, item)=>{
        let newResult = {}
        newResult['user'] = item.sender.id;
        newResult['message'] = handleGetMessage(item.message);
        newResult['user_image'] = item.user_image;
        newResult['timestamp'] = item.timestamp

        acumulator.push(newResult)
        return acumulator

    }, [])
    
}
const getFormatedChatUsers  = (chatUsers, onlineUserList)=>{
    const userId = getUserId();
    /*Hàm reduce được sử dụng để lặp qua từng phần tử trong mảng chatUsers. Giá trị ban đầu của acumulator là một mảng rỗng []. 
    Hàm này sẽ trả về một mảng mới sau khi duyệt qua toàn bộ chatUsers. */
    return chatUsers?.reduce((acumulator, item) =>{
        if (item.type ==="DM" || item.type === "SELF")
        {
            let newResult = {}
            newResult['roomId'] = item.room_id
            let member = null;
            for (let user of item.connected_users)
            {
                if(user.id !== userId || item.type ==="SELF")
                {
                    member = user;
                }
            }
            if (member)
            {
                newResult["name"] = member.first_name + " " + member.last_name
                newResult["name"] = member.username
                newResult["image"] = member.user_image
                newResult['id'] = member.id
                newResult['isOnline'] = onlineUserList?.includes(member.id)
                newResult['latestMsg'] = item.latest_msg.message
            }
            acumulator.push(newResult)
            return acumulator
        }
        return acumulator
    }, [])
}

const getFormatedOneChatUser = (chatUser, onlineUserList) => {
    const userId = getUserId();
    let newResult = {}
    newResult['roomId'] = chatUser.room_id
    let member = null;
    for (let user of chatUser.connected_users)
    {
        if(user.id !== userId || chatUser.type ==="SELF")
        {
            member = user;
        }
    }
    if (member)
    {
    
        newResult["name"] = member.username
        newResult["image"] = member.user_image
        newResult['id'] = member.id
        newResult['isOnline'] = onlineUserList?.includes(member.id)
        newResult['latestMsg'] = chatUser.latest_msg.message
    }

    return newResult

}

const getUserId = () =>{
    try{

        let access_token = JSON.parse(localStorage.getItem('access_token'));
        if (access_token)
            {
                let decodedToken = jwt_decode(access_token)
                return decodedToken.id
            }
    }catch(e){
        return null;
    }
    return null;
}

const getUserImage = () =>{
    try{
        let access_token = JSON.parse(localStorage.getItem('access_token'));
        if(access_token)
        {
            let decodedToken = jwt_decode(access_token)
            return decodedToken.user_image
        }
    }catch(e){
        return null;
    }
    return null
}

const getActiveChatId = (match) =>{
    return match && match.chatId ? match.chatId : null;

}

const getToken = (token) =>{
    try{
        let access_token = JSON.parse(localStorage.getItem(token))
        if(access_token)
        {
            let decodedToken = jwt_decode(access_token)
            return decodedToken.id
        }
    }
    catch(err)
    {

        return null;
    }
}

const getAuthUsername = ()=>{
    try{
        let access_token = JSON.parse(localStorage.getItem('access_token'))
        if(access_token)
        {
            let decodedToken = jwt_decode(access_token)
            return decodedToken.username
        }

    }
    catch(err){
        return null;
    }
}

const encryptMessage = (message) =>{
    console.log("asdas key:", process.env.REACT_APP_SECRET_MESSAGE_KEY)
    return encrypt(message, process.env.REACT_APP_SECRET_MESSAGE_KEY)
}

const decryptMessage = (token) =>{
    return decrypt(token, process.env.REACT_APP_SECRET_MESSAGE_KEY)
}

const handleGetMessage = (message) =>{
    if (message[0] === '#')
    {
        return decryptMessage(message)
    }
    return message
}

const CommonUtils = {
    getActiveChatId: getActiveChatId,
    getUserId: getUserId,
    getFormatedChatUsers: getFormatedChatUsers,
    getFormatedOneChatUser: getFormatedOneChatUser,
    getFormattedMessages: getFormattedMessages,
    getToken: getToken,
    getAuthUsername: getAuthUsername,
    getUserImage: getUserImage,
    encryptMessage: encryptMessage,
    decryptMessage: decryptMessage,
    handleGetMessage: handleGetMessage,
}

export default CommonUtils;