
import ChatApp from "./pages/ChatApp.jsx";
import Home from "./pages/Home.jsx";
import Login from "./pages/Login.jsx";
import Individual from "./pages/Individual.jsx";

// import Login from "./pages/Login.jsx";

import Register from "./pages/Register.jsx";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import DetailPost from "./components/DetailPost.jsx";
import CreatPost from "./components/CreatPost.jsx";
import Menu from "./components/Menu.jsx";
import Subnav from "./components/Subnav.jsx";
import NavbarIndividual from "./components/NavbarIndividual.jsx";
import Comment from "./components/Comment.jsx";
import AppPaths from "./lib/appPaths.js";

import "./style.scss"
import "./themify-icons/themify-icons.css"
import NavbarHome from "./components/NavbarHome.jsx";
import HomePage from "./components/HomePage.jsx";
import Test from "./pages/test.jsx";
import { Component } from "react";
import PrivateLiveVideoCall2 from "./pages/VideoCall/PrivateLiveVideoCall2.js";
import IndividualAuth from "./components/IndividualAuth.jsx";
import Main  from "./pages/Main.jsx";
class App extends Component {
  render(){
    
      return (
            <Router>
                <Routes>
                  <Route path={AppPaths.LOGIN} element={<Login/>}/>
                  <Route path={AppPaths.SIGN_UP} element={<Register/>}/>
                  <Route path="/videotest" element={<PrivateLiveVideoCall2/>}/>
                  <Route path={AppPaths.HOME} element={<Home/>}/>
                  <Route path={AppPaths.DEFAULT_CHAT} element={<ChatApp/>}/>
                  <Route path={AppPaths.CHAT_ROOM} element={<ChatApp/>}/>

                  <Route path="Individual" element={<Individual/>}>
                    <Route path="CreatPost1" element={<CreatPost />} />
                    <Route path="IndividualAuth" element={<IndividualAuth />} />
                  </Route> 
                  
                  <Route path="/posts/:postId/" element={<DetailPost/>} />

                </Routes>
                
                  {/* <div className="home">
                    <div className="container">
                      
                    <Menu />
                    <Routes>
                        <Route path={AppPaths.HOME} element={<Home/>}/>
                        <Route path={AppPaths.DEFAULT_CHAT} element={<ChatApp/>}/>
                        <Route path={AppPaths.CHAT_ROOM} element={<ChatApp/>}/>

                        <Route path="/Individual" element={<Individual/>}/> 
                        <Route path="/posts/:postId/" element={<DetailPost/>} />
                    </Routes>
                      
                    </div>
                  </div> */}
  
              </Router>
      
        // <Test/>
        // <Individual/>
      );
    }
  }
  
export default App;