import axios from "axios";
import ServerUrl from "../api/serverUrl";
import AppPaths from "../lib/appPaths";
import ApiEndpoints from "../api/apiEndpoints";

const signup = async (username, password, email, firstName, lastName, address="", phoneNumber="") =>{
    const resquestBody = {
        email:email,
        username:username,
        password:password,
        first_name:firstName,
        last_name:lastName,
        address: address,
        phone_number:phoneNumber
    }
    return await axios
    .post(ServerUrl.BASE_URL + ApiEndpoints.SIGN_UP_URL, resquestBody)
    .then((response) => {
        if (response.statusCode === 200)
        {
            console.log(response);
        }
        return response.data;
    })
}

const login = async (username, password) => {
    return await axios
    .post(ServerUrl.BASE_URL + ApiEndpoints.LOGIN_URL, {
        username,
        password,
    }).then((response) => {
        console.log(response);
        if (response.data)
        {
            localStorage.setItem("access_token", JSON.stringify(response.data.data.access_token))
        }
        return response.data;
    })
}

const logout = () =>{
    localStorage.removeItem("access_token")

}

const AuthService = {
    signup:signup,
    login:login,
    logout:logout,

}

export default AuthService