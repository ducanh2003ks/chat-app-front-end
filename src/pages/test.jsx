import React, { useState, useRef } from 'react';

function MusicPlayer() {
  const [audioFile, setAudioFile] = useState(null);
  const audioElement = useRef(null);

  const onAudioChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      const audioURL = URL.createObjectURL(file);
      setAudioFile(audioURL);
    }
  };

  const playAudio = () => {
    if (audioElement.current) {
      audioElement.current.play();
    }
  };

  return (
    <div>
      <label htmlFor="audio-upload" className="custom-file-upload">
        Tải tệp âm thanh lên
      </label>
      <input type="file" id="audio-upload" accept="audio/*" onChange={onAudioChange} style={{ display: 'none' }} />
      {audioFile && (
        <>
          <audio ref={audioElement} src={audioFile} controls />
          <button onClick={playAudio}>Bắt đầu chơi</button>
        </>
      )}
    </div>
  );
}

export default MusicPlayer;
