import React from "react";
import { Route, Routes, BrowserRouter as Router ,Link} from 'react-router-dom';
import Menu from "../components/Menu.jsx";
import Subnav from "../components/Subnav.jsx";
import IndividualAuth from "../components/IndividualAuth.jsx"
import NavbarIndividual from "../components/NavbarIndividual.jsx";
import CreatPost from "../components/CreatPost.jsx";

import { Outlet } from "react-router-dom";


const Individual = () => {
    return (
    <>
        <div className="home">
            <div className="container">
            <Menu/>
            <NavbarIndividual />
    
            {/* <CreatPost/> */}
            <Outlet></Outlet>
            </div>
        </div>
    </>

    )
  }
  
export default Individual;


//   const Home = () => {
//     return (
//         <Register/>
//     )
//   }
  
//   export default Home