import React from "react";
// import Register from "./Register.jsx";
import Menu from "../components/Menu.jsx";
import NavbarChat from "../components/NavbarChat.jsx";
import ChatPage from "../components/ChatPage.jsx";
import Subnav from "../components/Subnav.jsx";
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { Component } from "react";
import { useParams } from "react-router-dom";
import ServerUrl from "../api/serverUrl.js";
import CommonUtils from "../utils/commonUtils.js";
import ApiEndpoints from "../api/apiEndpoints.js";
import {w3cwebsocket as WebSocket} from 'websocket'
import SocketActions from "../lib/socketActions.js";
import { AddRoomChatForm } from "../components/AddRoomChatForm.jsx";



const ChatApp = (props) => {
  const [currentChattingMember, setCurrentChattingMember] = useState({});
  const [onlineUserList, setOnlineUserList] = useState([]);
  const [isOpenChatRoom, setIsOpenChatRoom] = useState(false);
  const [isOpenAddChatRoomForm, setIsOpenAddChatRoomForm] = useState(false)
  
  const match =  useParams()

  // if (onlineUserList.length === 0) {
  //   return (
  //     <div>Loading...</div>
  //   )
  // }
  
    return (
       
      <>
      <div className="home">
        <div className="container">
              <Menu/>
              {isOpenAddChatRoomForm &&
              <AddRoomChatForm setIsOpenAddChatRoomForm={setIsOpenAddChatRoomForm}/>
              }
              <NavbarChat
              setCurrentChattingMember={setCurrentChattingMember}
              onlineUserList={onlineUserList}
              match={match}
              setIsOpenChatRoom = {setIsOpenChatRoom}
              setIsOpenAddChatRoomForm={setIsOpenAddChatRoomForm}
              />
          
              <ChatPage
              setOnlineUserList={setOnlineUserList}
              currentChattingMember={currentChattingMember}
              match={match}
              />
             </div>
        </div>
        </>
     
    )
  }
  
export default ChatApp;