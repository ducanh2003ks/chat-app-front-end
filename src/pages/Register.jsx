import React ,{useState} from "react";
import Add from "../imgs/addAvatar.png";
import {useNavigate} from "react-router-dom";
import AppPaths from "../lib/appPaths";
import AuthService from "../services/auth.service";

const Register = () => {
  const navigate = useNavigate()
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password1, setPassword1] = useState("")
  const [password2, setPassword2] = useState("")
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [address, setAddress] = useState("")
  const [phoneNumber, setPhoneNumber] = useState("")

  const redirectToLoginPage = () =>{
    navigate(AppPaths.LOGIN);
  }

  const handleSignup = async (e) =>{
    e.preventDefault();
    try{
      if (password1 !== password2)
      {
        alert("Password not match!")
        return;
      }else{
        await AuthService.signup(username, password1, email, firstName, lastName, address, phoneNumber)
        .then(() =>{
          alert("Dang ki thanh cong")
          window.location.href = AppPaths.LOGIN
        }, 
        (error) =>{
          console.log(error)
        }
        )
      }
    }catch(error)
    {
      console.error("error", error)
    }
  }

  return (
    <div className="formContainer">
      <div className="formWrapper">
        <span className="desc-register">Chào mừng bạn</span>
        <span className="title">Hãy đăng ký tài khoản mới</span>
        <form>
          <input className="input-fix" required type="text" 
          placeholder="Tên Tài Khoản" 
          value={username}
          onChange={(e) =>{
            setUsername(e.target.value);
          }}
          />
          <input className="input-fix" required type="email" 
          placeholder="Email" 
          value={email}
          onChange={(e) =>{
            setEmail(e.target.value);
          }}
          />
          <div style={{display:"flex"}}>
            <input style={{width:"165px",marginRight:"12px"}} className="input-fix" type="text" 
            placeholder="Họ" required
            value={firstName}
            onChange={(e)=>{
              setFirstName(e.target.value)
            }}
            />
            <input  style={{width:"174px"}}  className="input-fix" type="text" 
            placeholder="Tên" required
            value={lastName}
            onChange={(e)=>{
              setLastName(e.target.value)
            }}
            />
          </div>
          <div style={{display:"flex"}}>
            <input  style={{width:"208px",marginRight:"12px"}} className="input-fix" type="text" 
            placeholder="Đia chỉ"
            value={address}
            onChange={(e)=>{
              setAddress(e.target.value)
            }}
            />
            <input style={{width:"132px"}}  className="input-fix" type="text" 
            placeholder="Số điện thoại" required
            value={phoneNumber}
            onChange={(e) =>{
              setPhoneNumber(e.target.value)
            }}
            />
          </div>
          
          <input className="input-fix" required type="password" 
          placeholder="Mật Khẩu" 
          value={password1}
          onChange={(e) =>{
            setPassword1(e.target.value)
          }}
          />
          <input className="input-fix" type="password" required 
          placeholder="Confirm password" 
          value={password2}
          onChange={(e) =>{
            setPassword2(e.target.value)
          }}
          />
          {/* <input className="input-fix" required type="password" placeholder="Xác Nhận Lại Mật Khẩu" /> */}
          <input className="input-fix" required style={{ display: "none" }} type="file" id="file" />
          <label htmlFor="file">
            <img className="img-add" src={Add} alt="" />
            <span>Thêm ảnh đại diện</span>
          </label>
          <button 
          className="btn-register"
          onClick={(e) =>{
            handleSignup(e)
          }}
          >
            Đăng ký
          </button>
        
        </form>
        <p style={{color:"white"}}>
            Bạn đã có tài khoản ? 
            <span className="clickDangKy" onClick={redirectToLoginPage}>
            Đăng nhập
            </span>
           
        </p>
      </div>
    </div>
  );
};

export default Register;