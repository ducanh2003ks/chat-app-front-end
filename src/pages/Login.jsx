import React from "react";
import { useState, useEffect } from "react";
import {Link, useNavigate} from 'react-router-dom'
import AuthService from "../services/auth.service"
import CommonUtils
 from "../utils/commonUtils";
 import ServerUrl from "../api/serverUrl";
 import AppPaths from "../lib/appPaths";
const Login = () => {

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()
  const handleSubmit = async (e) =>{
    e.preventDefault()
      try{
        await AuthService.login(username, password)
        .then(() =>{
          // navigate("/");
        window.location.href = AppPaths.HOME;
        },
        (error) =>{
          console.log(error)
        })
      }catch(err){
        console.log(err)
      }
      
  }
  const redirectToSignUpPage = () =>{
    navigate(AppPaths.SIGN_UP)
  }

  return (
    <div className="formContainer">
      <div style={{width:"485px"}} className="formWrapper">
        <span className="desc-register">Chào mừng bạn</span>
        <span className="title">Hãy đăng nhập tài khoản</span>
        <form onSubmit={handleSubmit}>
          <input onChange={(e)=>{ setUsername(e.target.value)}} className="input-fix" required type="text" placeholder="Tên Tài Khoản" />
          <input onChange={(e)=>{ setPassword(e.target.value)}} className="input-fix" required type="password" placeholder="Mật Khẩu" />
          <button type="submit" style={{marginTop:"32px"}} className="btn-register">Đăng nhập</button>
        </form>
        <p className="desc">
            Bằng việc tiếp tục, bạn đã đồng ý với
            mọi Điều khoản sử dụng, Chính sách bảo mật của chúng tôi 
        </p>
        <p style={{color:"white"}}>
            Bạn chưa có tài khoản ?
            <span className="clickDangKy" onClick={redirectToSignUpPage}>
              Đăng ký
            </span>
        </p>
      </div>
    </div>
  );
};


export default Login;