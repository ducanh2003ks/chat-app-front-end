import React, { Component } from "react";
import { useNavigate } from "react-router-dom";
import AppPaths from "../../lib/appPaths";
import Constants from "../../lib/constants";
import CommonUtils from "../../utils/commonUtils";
import ServerUrl from "../../api/serverUrl";

const AuthRequired = (WrappedComponent) => {
  const WithAuthentication = (props) => {
    const navigate = useNavigate();

    if (CommonUtils.getToken(Constants.TOKEN)) {
      return <WrappedComponent {...props} />;
    } else {
      window.location.href = AppPaths.LOGIN
      return null;
    }
  };

  return WithAuthentication;
};

export default AuthRequired;