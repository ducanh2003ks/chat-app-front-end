import ChatApp from "./ChatApp.jsx";
import Home from "./Home.jsx";
import Login from "./Login.jsx";
import Individual from "./Individual.jsx";
import Register from "./Register.jsx";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import DetailPost from "../components/DetailPost.jsx";
import Menu from "../components/Menu.jsx";
import AppPaths from "../lib/appPaths.js";

const Main = ({match}) =>{
    return(

        <Routes>
            <Route path={AppPaths.HOME} element={<Home/>}/>
            <Route path={AppPaths.DEFAULT_CHAT} element={<ChatApp/>}/>
            <Route path={AppPaths.CHAT_ROOM} element={<ChatApp/>}/>

            <Route path="/Individual" element={<Individual/>}/> 
            <Route path="/posts/:postId/" element={<DetailPost/>} />
        </Routes>
            
    )
}
export default Main;