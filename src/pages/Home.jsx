import React from "react";
import { Routes, Route, Link } from 'react-router-dom';
import Menu from "../components/Menu.jsx";
import Subnav from "../components/Subnav.jsx";
import NavbarHome from "../components/NavbarHome.jsx";
import DetailPost from "../components/DetailPost.jsx";
import HomePage from "../components/HomePage.jsx";
import { useState, useEffect } from "react";
import axios from "axios";
import AuthRequired  from "./auth/AuthRequired.jsx";
import ApiConnector from "../api/apiConnector.js";
import ApiEndpoints from "../api/apiEndpoints.js";
import CommonUtils from "../utils/commonUtils.js";
const Home = () => {
  console.log("Home");
  const [userAuth, setUserAuth] = useState(null)
  const getAuthUser = async () =>{
    const response = await ApiConnector.sendGetRequest(ApiEndpoints.AUTH_USER_PROFILE)
    setUserAuth(response)
  }
  useEffect(() =>{
    getAuthUser()
  }, [])
  
  if (!userAuth){
    return (
      <div>Loading ...</div>
    )
  }
  
  return (
    <>
     <div className="home">
        <div className="container">
        <Menu/>
        <NavbarHome />
        <HomePage />
        <Subnav userAuth={userAuth}/>
        </div>
    </div>
    </>

  );
};

export default AuthRequired(Home);