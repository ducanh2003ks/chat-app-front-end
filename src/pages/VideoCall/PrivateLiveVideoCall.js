import React, {useRef, createRef, useEffect, useContext, useState, useDebugValue} from "react";
import { w3cwebsocket as Websocket } from "websocket";
import { useParams } from "react-router-dom";
import ServerUrl from "../../api/serverUrl";
import CommonUtils from "../../utils/commonUtils";
import '../VideoCall/PrivateLiveVideoCall.css'
import profile from '../../imgs/profile.png'
import Tooltip from '@mui/material/Tooltip'
import Fab from '@mui/material/Fab'
import AppPaths from "../../lib/appPaths";


const PrivateLiveVideoCall = ({setIsVideoCallActive, caller, otherUser, callSocket, isCaller})=>{
    let pcConfig = {
      'iceServers':[
        {
          "url":"stun:stun.jap.bloggernepal.com:5349"
        },
        {
          "url": "turn:turn.jap.bloggernepal.com:5349",
                "username": "guest",
                "credential": "somepassword"
        },
        {
          "url": "stun:stun.l.google.com:19302"
        }
      ]
    }
    console.log(otherUser)
    const callerName = caller;
    const otherName = otherUser;

    const localVideoRef = useRef(null);
    const remoteVideoRef = useRef(null);

    const remoteRTCMessage = useRef(null);
    const [iceCandidatesFromCaller, setIceCandidatesFromCaller] = useState([]);
    const peerConnection = useRef(null);
    const remoteStream = useRef(null);
    const localStream = useRef(null);

    let callInProgress = false;

    const tracks = useRef(null)

    const handleSetUpStream = async () =>{
      let constraints = {
        video:true,
        audio:true
      }
      return await navigator.mediaDevices.getUserMedia(constraints)
      .then(stream =>{
        console.log("stream: " + stream)
        localStream.current = stream

        console.log("localVideo: ", stream)
        localVideoRef.current.srcObject =stream;
        return createConnectionAndAddStream()
      })
      .catch(error => {
        console.error("error:", error)
      })


    }

    const createConnectionAndAddStream = ()=>{
      createPeerConnection()
      peerConnection.current.addStream(localStream.current)
      return true
    }

    const sendICECandidate = (data)=>{
      console.log("Send ICE candidate")
      callSocket.send(JSON.stringify({
        type: "ICEcandidate",
        data:data
      }));
    }

    const handleICECandidate = (event) =>{
      if(event.candidate)
      {
        console.log("local ICE candidate")
        sendICECandidate({
          user:otherUser,
          rtcMessage:{
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
          }
        })
      }else{
        console.log("end of candidates")
      }
    }

    const handleRemoteStreamAdded = (event)=>{
      console.log("Remote stream added")
      remoteStream.current = event.stream;
      remoteVideoRef.current.srcObject = remoteStream.current

    }

    const handleRemoteStreamRemoved = (event) =>{
      console.log("Stream removed")
      remoteStream.current = null;
      localVideoRef.current.srcObject = null
    }
    const createPeerConnection = () =>{
      try{

        peerConnection.current = new RTCPeerConnection(pcConfig)
        console.log("peer:", peerConnection)
        peerConnection.current.onicecandidate = handleICECandidate;
        peerConnection.current.onaddstream = handleRemoteStreamAdded;
        peerConnection.current.onremovestream = handleRemoteStreamRemoved;
        console.log("set onicecandidate")

      }catch(e)
      {
        console.error(e)
        return;
      }
    }




    const sendCall = (data) =>{
      // to send a call
      console.log("send call");
      callSocket.send(JSON.stringify({
        type: "call",
        data
      }))
      document.getElementById("calling").style.display = "block"
      document.getElementById("calling").style.position = "absolute"
      document.getElementById("calling").style.zIndex = "99"
      document.getElementById("answer").style.display = "none";

    }

    const processCall = (username) =>{
 
      console.log("peer after click call:", peerConnection)
      peerConnection.current.createOffer((sessionDescription) =>{
        peerConnection.current.setLocalDescription(sessionDescription);
        sendCall({
          name: username,
          rtcMessage: sessionDescription
        })
      }, (error) =>{
        console.log("error")
      })
    } 

    const answerCall = (data) =>{
      // to answer call
      callSocket.send(JSON.stringify({
        type: 'answer_call',
        data
      }))
      callProgress()
    }

    const processAccept = () =>{
      console.log("set new remote description for answerer")
      peerConnection.current.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage.current))
      peerConnection.current.createAnswer((sessionDescription) =>{
        peerConnection.current.setLocalDescription(sessionDescription)

          if (iceCandidatesFromCaller.length > 0)
          {
            // Having issues with call bot being precessed in real wrold (internet, not local)
            // so solution is to push iceCandidates I received after the call arrive, push it and, once we accept
            // add it as ice candidate
            // if the offer rtc message contains all the ICE candidates we can ignore this
            for (let i = 0; i < iceCandidatesFromCaller.length; i++) {
              let candidate = iceCandidatesFromCaller[i];
              console.log("ice candidatr added from queue")
              try{
                peerConnection.current.addIceCandidate(candidate).then(done=>{
                  console.log(done)
                }).catch(error=>{
                  console.log(error)
                })
              }catch(error)
              {
                console.log("error:", error)
              }
    
            }
            setIceCandidatesFromCaller([]);
            console.log("ICE candidate queue cleared")

          }else{
            console.log("No ice candiadate in queue")
          }
          answerCall({
            caller: otherUser,
            rtcMessage: sessionDescription,
          })
      }, error =>{
        console.log("error: " + error)
      })

    }

    const call = () =>{

      handleSetUpStream()
      .then(bool=>{
        processCall(otherUser)
      })


    }
    const answer = () =>{
      handleSetUpStream()
      .then(bool =>{
        processAccept();
      })
    }

    const callProgress = () =>{
      document.getElementById("videos").style.display = "block";
      document.getElementById("otherUserNameC").innerHTML = otherUser
      document.getElementById("inCall").style.display = "block"
      document.getElementById("answer").style.display = "none";
      
    }

    const stop = () =>{
      // track bao gồm các đầu vào, đầu ra như audio (microphone, mic macbook,...), video
      localStream.current.getTracks().forEach(track=>track.stop())
      localStream.current = null;
      callInProgress = false
      peerConnection.current.close()// gọi tới hàm onremovestream
      peerConnection.current = null;
      document.getElementById("answer").style.display = "none"
      document.getElementById("inCall").style.display = "none"
      document.getElementById("calling").style.display = "none"
      setIsVideoCallActive(false)
      window.location.reload()
    }

    // Trước khi unload thì xóa tracks trong localStream, xóa peerConnection
    window.onbeforeunload = function () {
      if(callInProgress)
      {
        stop();
      }
    }

    const onNewCall = (data)=>{
      // when other call you
      // show answer button
      console.log("no vo on new call")
      remoteRTCMessage.current = data.rtcMessage;

      document.getElementById('callerName').innerHTML = otherUser;
      document.getElementById('answer').style.display = 'block';
      document.getElementById('answer').style.position = 'absolute';
      document.getElementById('answer').style.zIndex = '99';


    }


    const onCallAnswered = (data) =>{
      // when other user accept our call
      remoteRTCMessage.current = data.rtcMessage;
      console.log("data rtc message:",data.rtcMessage);
      console.log("remote RTC, ",remoteRTCMessage.current); 
      peerConnection.current.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage.current))

      document.getElementById("calling").style.display = "none";
      console.log("Call started. They answered")

      callProgress()
    }

    const onICECandidate = (data) =>{
      console.log("GOT ICE candidate")
      let message = data.rtcMessage

      let candidate = new RTCIceCandidate({
        sdpMLineIndex: message.label,
        candidate: message.candidate
      })
      console.log("candidate", candidate)
      if(peerConnection.current)
       {
        console.log("ICE candidate added")
        peerConnection.current.addIceCandidate(candidate)

       }
       else{
        console.log("ICE candidate pushed")
        setIceCandidatesFromCaller((prevCandidates)=>{
          return [...prevCandidates, candidate]
        })
       }
    }



    callSocket.onopen = ()=>{
      console.log("connection established")
    }
    
    callSocket.onmessage = (event) =>{
      console.log("event:",event.data)
      let response = JSON.parse(event.data)
      let type = response.type

      if(type == 'connection'){
        console.log(response.data.message)
        
      }

      if(type == 'call_received'){
        console.log("no vo call received")
        onNewCall(response.data)
      }

      if(type == 'call_answered'){
        onCallAnswered(response.data)
      }
      if(type == 'ICEcandidate')
      {
        console.log("no vo icecandidate")
        onICECandidate(response.data)
      }
      if(type == 'decline')
      {
        console.log("no vo decline:", response)
        alert(`opponent declined your call`)
        document.getElementById("answer").style.display = "none"
        document.getElementById("inCall").style.display = "none"
        document.getElementById("calling").style.display = "none"
        localStream.current.getTracks().forEach(track=>track.stop())
        localStream.current = null;
        callInProgress = false
        peerConnection.current.close()// gọi tới hàm onremovestream
        peerConnection.current = null;
        setIsVideoCallActive(false)
        window.location.reload()
      }

    }
    useEffect(() =>{

      document.getElementById("inCall").style.display = "none";
      document.getElementById("calling").style.display = "none";
      document.getElementById("videos").style.display = "none";
      document.getElementById("answer").style.display = "none";
      if(isCaller)
      {
        call();
      }
  
    },[])

    const [isVideoMuted, setIsVideoMuted] = useState(false);    
    const [isAudioMuted, setIsAudioMuted] = useState(false);    
    const muteVideo = () =>{
      const stream = document.getElementById("localVideo").srcObject;
      if (!stream.getVideoTracks()[0]) return;

      stream.getVideoTracks()[0].enabled = !stream.getVideoTracks()[0].enabled;
      setIsVideoMuted(!stream.getVideoTracks()[0].enabled)
    }

    const muteAudio = () =>{
      const stream = document.getElementById('localVideo').srcObject;
      if (!stream.getAudioTracks()[0]) return;
      stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled;
      setIsAudioMuted(!stream.getAudioTracks()[0].enabled)

    }

    const decline = () =>{
      callSocket.send(JSON.stringify(
        {
          type:"decline",
          data: {
            user: CommonUtils.getAuthUsername(),
            caller: otherUser
          }
          
        }
      ))
      document.getElementById("answer").style.display = "none"
      document.getElementById("inCall").style.display = "none"
      document.getElementById("calling").style.display = "none"
      window.location.reload()
    }

    window.addEventListener("reload", () =>{

    })
    return (
    <div className="container-video-call">
      <div id="answer">
        <div className="incomingWrapper">
          <div className="itemWrapper">
            <h2>Incoming Call</h2>
          </div>
          <div className="itemWrapper">
            <img id="profileImageA" style={{ padding: '30px', width: '140px', height: '140px' }} src={profile} alt="" />
          </div>
          <div className="itemWrapper">
            <h2 style={{ lineHeight: '0px' }}><span id="callerName">{otherUser}</span></h2>
          </div>
          <div className="itemWrapper" style={{ display: 'flex', flexDirection: 'row', marginBottom: '20px' }}>
            <button onClick={answer} className="actionButton">Answer</button>
            <button onClick={decline} className="declineButton">Decline</button>
          </div>
        </div>
      </div>
      <div id="calling">
        <div className="incomingWrapper">
          <div className="itemWrapper">
            <h2>Calling</h2>
          </div>
          <div className="itemWrapper">
            <img id="profileImageCA" style={{ padding: '30px', width: '140px', height: '140px' }} src={profile} alt="" />
          </div>
          <div className="itemWrapper">
            <h3 style={{ lineHeight: '0px' }}><span id="otherUserNameCA">{otherUser}</span></h3>
          </div>
        </div>
      </div>
      <div id="inCall">
        <div className="incomingWrapper">
          <div className="itemWrapper">
            <h3>On Call With</h3>
            <h2 style={{ lineHeight: '0px' }}><span id="otherUserNameC">{otherUser}</span></h2>
          </div>
        </div>
      </div>
      <div id="videos">
        <div style={{ position: 'absolute', top: '0', right: '0', padding: '20px', paddingTop: '20px' }}>
          <video ref={localVideoRef} width="100px" id="localVideo" autoPlay muted playsInline></video>
        </div>
        <div style={{position:"fixed",}} id="remoteVideoDiv">
          <video ref={remoteVideoRef} style={{ width: '500px', zIndex:'99', position:'relative'}} id="remoteVideo" autoPlay playsInline></video>
        </div>
        <div className="floatingButtons">

          <Tooltip className="floatingButton">
            <Fab onClick={muteAudio}>
              {
                isAudioMuted ?(
                  "audio off"
                ):(
                  "audio on"
                )
              }
            </Fab>
          </Tooltip>
          <Tooltip className="floatingButton">
            <Fab onClick={muteVideo}>
            {
                isVideoMuted ?(
                  "video off"
                ):(
                  "video on"
                )
            }
            </Fab>
          </Tooltip>
          <Tooltip className="floatingButton">
            <Fab onClick={stop}>
              End call
            </Fab>
          </Tooltip>

        </div>
      </div>
     
      <div style={{ flexGrow: '1' }}></div>
    </div>

    );
};

export default PrivateLiveVideoCall